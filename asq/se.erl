-module(se).

-include_lib("eunit/include/eunit.hrl").

-export([t/1]).

-record(mail, {from, to, label}).

%% ASQ Language's basic Grammar
%% ‹exp› := ‹label› ':' ‹grbg› ' ' ‹exp›
%%       | 𝛆
%% ‹label› := 'from' | 'to' | 'label'
%% ‹gr› := [^ \t]+


t(Q) when is_list(Q) ->
    %% Must be suffixed by ‹separator(ie: ' ')› then [^ ].
    Q1 = list_to_binary(Q),
    Query = <<Q1/binary, " .">>, %% ' .' will do.
    Tuples = [#mail{from= <<"1">>,to= <<"2">>,label= <<"3">>},
              #mail{from= <<"2">>,to= <<"3">>,label= <<"4">>},
              #mail{from= <<"3">>,to= <<"4">>,label= <<"5">>}],
    apply_query(Query, Tuples).

t_test() ->
    [] = t("label:5 to:3"),
    [{mail,<<"3">>,<<"4">>,<<"5">>}] = t("label:5 to:4").


apply_query(<<"label:", ToLex/binary>>=M, Tuples) ->
    io:format("got '~p' ~p~n", [M, label]),%
    reduce(label, ToLex, Tuples);
apply_query(<<"to:", ToLex/binary>>=M, Tuples) ->
    io:format("got '~p' ~p~n", [M, to]),%
    reduce(to, ToLex, Tuples);
apply_query(<<"AND", ToLex/binary>>=M, Tuples) ->
    io:format("got '~p' ~p~n", [M, 'AND']),%
    reduce('AND', ToLex, Tuples);
apply_query(_, Tuples) ->
    io:format("Mails: {from, to, label}~n"),
    Tuples.


reduce('AND', ToLex, Tuples) ->
    {_Token, Rest} = look_ahead(ToLex),
    apply_query(Rest, Tuples);

reduce(Field, ToLex, Tuples) ->
    io:format("got '~p' ToLex~n", [ToLex]),%
    {Token, Rest} = look_ahead(ToLex),
    io:format("got '~p' Token~n", [Token]),%
    PartialRes = [T || T = #mail{Field=Token} <- Tuples],
    io:format("got '~p' PartialRes~n", [PartialRes]),%
    apply_query(Rest, PartialRes).


%% look_ahead(<<":", Rest/binary>>) -> {colon, Rest};
look_ahead(Bin) when is_binary(Bin) ->
    [Lhs, Rhs] = binary:split(Bin, [<<" ">>, <<"	">>]),
    {Lhs, Rhs}.
